# CPB

## What is it?

The chaotic portable builder is a simple, chaotic-v4 infrastructure-compatible local/test builder using podman. It utilizes the exact same code that is used to build packages on the chaotic-v4 infrastructure, allowing package building to be easily tested locally/on a remote server and allowing quick iterative development.

## How do you use it?

The easiest way to get started with CPB is to use `nix` to install all required dependencies using the command `nix-shell`. Otherwise, the following dependencies are required: `coreutils podman expect`

Next, create a new directory (or use the one this file is in) and insert a chaotic-v4 compatible pkgbuilds repo into the folder named `pkgbuilds`. Lastly, run `cpb build [pkgbase]` (or `./cpb build [pkgbase]` if you are not using nix-shell), replacing `[pkgbase]` with the name of the folder inside the `pkgbuilds` directory (which should be equal to the pkgbase of the package you are building).

Done! CPB will now automatically pull the chaotic-v4 builder container, build the package, sign it with a newly generated key and add it to a local package repository. You can also build entire dependency chains of packages like this, because packages from the local repository are available to be installed on demand.

## Interested?

Chaotic-v4 is a full-blown infrastructure from the creators of Chaotic-AUR and Garuda Linux for automated repository management. Check it out: https://gitlab.com/garuda-linux/tools/chaotic-manager
