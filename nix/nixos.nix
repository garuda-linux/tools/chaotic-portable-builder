{ pkgs, ... }:
let
    cpb = pkgs.callPackage ./default.nix {};
in
{
    environment.systemPackages = with pkgs; [
        cpb
        podman
        fuse-overlayfs
    ];

    virtualisation.containers.enable = true;
}