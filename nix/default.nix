{
  lib,
  stdenvNoCC,
  makeWrapper,
  coreutils,
  podman,
  expect,
  fuse-overlayfs,
  skopeo,
}: let
  inherit (lib) makeBinPath;
in
  stdenvNoCC.mkDerivation rec {
    ## Metadata
    name = "chaotic-portable-builder";
    version = "1.0";

    src = ../cpb;

    nativeBuildInputs = [makeWrapper];

    buildInputs = [coreutils podman expect fuse-overlayfs skopeo];

    installPhase = ''
      install -Dm755 $src $out/bin/cpb
      wrapProgram $out/bin/cpb --prefix PATH : '${makeBinPath buildInputs}'
    '';

    dontUnpack = true;
  }