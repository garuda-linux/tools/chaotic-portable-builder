{ pkgs ? import <nixpkgs> {} }:
let
  cpb = pkgs.callPackage ./nix/default.nix {};
in
pkgs.mkShell {

  buildInputs = [
    cpb
    pkgs.podman  # Docker compat
    pkgs.skopeo  # Interact with container registry
    pkgs.fuse-overlayfs  # CoW for images, much faster than default vfs
  ];

  shellHook = ''
    if [ ! -f /etc/containers/policy.json ] && [ ! -f ~/.config/containers/policy.json ]; then
      echo "No podman policy.json found. Installing default to home folder."
      install -Dm755 ${pkgs.skopeo.src}/default-policy.json ~/.config/containers/policy.json
    fi
  '';
}
